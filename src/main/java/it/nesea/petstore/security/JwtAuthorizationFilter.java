package it.nesea.petstore.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import it.nesea.petstore.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Slf4j
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

  public JwtAuthorizationFilter(AuthenticationManager authenticationManager) {
	super(authenticationManager);
  }


  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
	 				throws IOException, ServletException {

    //log.info("in JwtAuthorizationFilter.doFilterInternal");

	String token = request.getHeader(SecurityConstants.TOKEN_HEADER);
	Set<String> blacklist = (Set) request.getSession().getAttribute("blacklist");
	if (blacklist != null && blacklist.contains(token)) {
	  response.setStatus(403);
	  return;
	}

	if (token!= null && !"".equals(token.trim()) && token.startsWith(SecurityConstants.TOKEN_PREFIX)) {

	  try {

		byte[] signingKey = SecurityConstants.JWT_SECRET.getBytes();

		Jws<Claims> parsedToken = Jwts.parser()
		   .setSigningKey(signingKey)
		   .parseClaimsJws(token.replace(SecurityConstants.TOKEN_PREFIX , ""));

		String username = parsedToken
		   .getBody()
		   .getSubject();

		List<GrantedAuthority> authorities = ((List<?>) parsedToken.getBody()
		   .get("rol")).stream()
		   .map(authority -> new SimpleGrantedAuthority((String) authority))
		   .collect(Collectors.toList());

		if (username != null && !"".equals(username.trim())) {

		  SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(username, null, authorities));

		}

	  } catch (ExpiredJwtException exception) {
		log.info("token JWT scaduto. {} - {}", token, exception.getMessage());
	  } catch (UnsupportedJwtException exception) {
		log.info("formato del token JWT non supportato. {} - {}", token, exception.getMessage());
	  } catch (MalformedJwtException exception) {
		log.info("formato del token JWT non valido. {} - {}", token, exception.getMessage());
	  } catch (SignatureException exception) {
		log.info("firma del token JWT non valida. {} - {}", token, exception.getMessage());
	  } catch (IllegalArgumentException exception) {
		log.info("token JWT non valorizzato. {} - {}", token, exception.getMessage());
	  }

	}

	filterChain.doFilter(request, response);

  }

}