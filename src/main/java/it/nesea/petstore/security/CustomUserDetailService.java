package it.nesea.petstore.security;

import it.nesea.petstore.entity.UserEntity;
import it.nesea.petstore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailService implements UserDetailsService {

  @Autowired
  UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

	Optional<UserEntity> optional = userRepository.findByUsername(username);

	User.UserBuilder builder = null;

	if (optional.isPresent()) {
	  UserEntity userEntity = optional.get();
	  builder = org.springframework.security.core.userdetails.User.withUsername(username);
	  //builder.password(new BCryptPasswordEncoder().encode(userEntity.getPassword()));
	  builder.password(userEntity.getPassword());
	  //builder.roles(userEntity.getRoles());
	  //builder.roles("ROLE_USER");
	  builder.roles("USER");

	} else {
	  throw new UsernameNotFoundException("User not found.");
	}

	return builder.build();
  }
}
