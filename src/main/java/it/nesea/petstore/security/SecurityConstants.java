package it.nesea.petstore.security;

public final class SecurityConstants {

  public static final String TOKEN_TYPE = "JWT";
  public static final String JWT_SECRET = "sdfsdfet340fvxviwerj304wefjsdkfwef0934wef0sdjfiwefjwe0dsse0fsefv";
  public static final String TOKEN_HEADER = "Authorization";
  public static final String TOKEN_PREFIX = "Petstore ";
  public static final String TOKEN_ISSUER = "Petstore";
  public static final String TOKEN_AUDIENCE = "Petstore users";
  public static final String AUTH_LOGIN_URL = "/user/login";

}
