package it.nesea.petstore;

import it.nesea.petstore.security.JwtAuthenticationFilter;
import it.nesea.petstore.security.JwtAuthorizationFilter;
import it.nesea.petstore.security.CustomUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class PetstoreApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter {


  @Autowired
  CustomUserDetailService customUserDetailService;


  @Override
  protected void configure(HttpSecurity http) throws Exception {
	http.csrf().disable()
	   .authorizeRequests()
	   .anyRequest().authenticated()
	   .and()
	   .addFilter(new JwtAuthenticationFilter(authenticationManager()))
	   .addFilter(new JwtAuthorizationFilter(authenticationManager()))
	   .sessionManagement()
	   .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }


  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
	auth.userDetailsService(customUserDetailService);
  }


  @Bean
  public PasswordEncoder passwordEncoder() {
	return new BCryptPasswordEncoder();
  }


}