package it.nesea.petstore.entity;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.NotEmpty;
//@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
//@RequiredArgsConstructor

@NoArgsConstructor
@AllArgsConstructor
//@Builder

@Entity
public class PetEntity {

  @Id
  @Positive @NotNull
  private Integer id;
  @NotEmpty
  private String category;
  @NotEmpty
  private String name;
  private String photoUrls;
  @NotEmpty
  private String tags;
  @NotEmpty
  private String status;

}
