package it.nesea.petstore.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

//@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
//@RequiredArgsConstructor

@NoArgsConstructor
@AllArgsConstructor
//@Builder

//@Entity
public class PetGrouppedStatusCount {

  private String status;
  private Long count;


}
