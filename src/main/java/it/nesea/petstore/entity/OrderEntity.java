package it.nesea.petstore.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.NotEmpty;
//import javax.persistence.TemporalType;
//import org.springframework.data.jpa.repository.Temporal;
//import java.sql.Timestamp;
//import java.sql.Timestamp;
import java.util.Date;

@NoArgsConstructor
@Data
@Entity
public class OrderEntity {

  @Id
  @Positive @NotNull
  private Integer id;
  @Positive @NotNull
  private Integer petId;
  @Positive @NotNull
  private Integer quantity;
  @Temporal(TemporalType.TIMESTAMP) @NotNull
  private Date shipDate;
  @NotEmpty
  private String status;
  @NotNull
  private Boolean complete;

}
