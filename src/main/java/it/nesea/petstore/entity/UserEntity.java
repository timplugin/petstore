package it.nesea.petstore.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Entity
public class UserEntity {

  @Id
  @Positive @NotNull
  private Integer id;
  @NotEmpty
  private String username;
  @NotEmpty
  private String firstName;
  @NotEmpty
  private String lastName;
  @Email
  private String email;
  @NotEmpty
  private String password;
  @NotEmpty
  private String phone;
  @Positive @NotNull
  private Integer userStatus;

}
