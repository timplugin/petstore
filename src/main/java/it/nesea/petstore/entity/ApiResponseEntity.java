package it.nesea.petstore.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@NoArgsConstructor
@Data
//@Entity
public class ApiResponseEntity {

  private Integer code;
  private String type;
  private String message;
  private String photoUrls;
  private String tags;
  private String status;

}
