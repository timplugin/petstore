package it.nesea.petstore.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@NoArgsConstructor
@Data
@Entity
public class TagEntity {

  @Id
  private Integer id;
  private String name;

}
