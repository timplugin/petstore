package it.nesea.petstore.repository;

import it.nesea.petstore.entity.PetEntity;
import it.nesea.petstore.entity.PetGrouppedStatusCount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Repository
public interface PetRepository extends CrudRepository<PetEntity, Integer> {

  Optional<PetEntity[]> findByStatusIn(List<String> status);

  Optional<PetEntity[]> findByTagsIn(String[] tags);

  @Query("SELECT " +
     "    new it.nesea.petstore.entity.PetGrouppedStatusCount(p.status AS status, COUNT(p) AS count) " +
     "FROM " +
     "    PetEntity p " +
     "GROUP BY " +
     "    p.status")
  List<PetGrouppedStatusCount> petGrouppedStatusCount();


  //Alternativa che usa una Map anziché un VO
  @Query("select new map(v.status as status, count(v) as count) from PetEntity v group by v.status")
  List<Map<String,String>> petGrouppedStatusCount2();

}
