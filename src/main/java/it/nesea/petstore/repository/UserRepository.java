package it.nesea.petstore.repository;

import it.nesea.petstore.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {

  public Optional<UserEntity> findByUsername(String username);

  public Integer deleteByUsername(String username);

}
