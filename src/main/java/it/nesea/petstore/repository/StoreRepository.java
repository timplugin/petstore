package it.nesea.petstore.repository;

import it.nesea.petstore.entity.OrderEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StoreRepository extends CrudRepository<OrderEntity, Integer> {

}
