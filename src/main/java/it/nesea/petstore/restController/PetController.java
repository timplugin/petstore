package it.nesea.petstore.restController;

import io.swagger.annotations.*;
import it.nesea.petstore.entity.PetEntity;
import it.nesea.petstore.repository.PetRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.*;


@RestController
@RequestMapping("/pet")
@Slf4j
@Validated
@Api(value = "/pet", description = "Tutto sui tuoi Pets" , tags = { "pet" })
public class PetController {


  @Autowired
  private PetRepository petRepository;


  @PostMapping
  @ApiOperation(value = "Aggiunge un nuovo pet allo store")
  @ApiResponses(value = {@ApiResponse(code = 405, message = "Dati non validi"), @ApiResponse(code = 406, message = "Pet già esistente")})
  public ResponseEntity createPet(@RequestBody @Valid @ApiParam("pet da aggiungere allo store") PetEntity pet ) {
      if (petRepository.existsById(pet.getId()))
        return new ResponseEntity("Pet già esistente", HttpStatus.NOT_ACCEPTABLE);
      petRepository.save(pet);
      return new ResponseEntity("Pet creato con successo", HttpStatus.CREATED);
  }


  @GetMapping(value = "/{petId}")
  @ApiOperation(value = "Trova un pet in base all'id")
  @ApiResponses(value = {@ApiResponse(code = 404, message = "Pet non trovato"),@ApiResponse(code = 400, message = "Id non valido")})
  public ResponseEntity findById(@PathVariable("petId") @ApiParam("id del pet da ricercare nello store")
                                   @Min(value = 1, message="petId deve essere maggiore di 1") Integer petId){

    Optional<PetEntity> optional = petRepository.findById(petId);
    if (!optional.isPresent())
      return new ResponseEntity("Pet non trovato", HttpStatus.NOT_FOUND);
    return new ResponseEntity(optional.get(), HttpStatus.OK);
  }


  @PutMapping
  @ApiOperation(value = "Aggiorna un pet esistente")
  @ApiResponses(value = {@ApiResponse(code = 404, message = "Pet non trovato"), @ApiResponse(code = 405, message = "Dati non validi")})
  public ResponseEntity updatePet(@RequestBody @ApiParam("pet da aggiornare nello store") @Valid PetEntity pet){
    if (!petRepository.existsById(pet.getId()))
      return new ResponseEntity("Pet non trovato", HttpStatus.NOT_FOUND);
    petRepository.save(pet);
    return new ResponseEntity("Pet aggiornato con successo", HttpStatus.OK);
  }


  @PostMapping(value = "/{petId}")
  @ApiOperation(value = "Aggiorna un pet tramite form-data")
  @ApiResponses(value = {@ApiResponse(code = 400, message = "Input non valido")})
  public ResponseEntity updatePetByForm(@PathVariable("petId") @ApiParam("id del pet pet da aggiornare nello store")Integer petId,
                                  @ApiParam("nuovo nome del pet")  @NotEmpty(message = "il nuovo nome non può essere vuoto") @RequestParam String petName ,
                                  @ApiParam("nuovo stato del pet") @NotEmpty(message = "il nuovo stato non può essere vuoto") @RequestParam String petStatus){
    Optional<PetEntity> optional = petRepository.findById(petId);
    if (!optional.isPresent())
      return new ResponseEntity("Pet non trovato", HttpStatus.NOT_FOUND);
    PetEntity pet = optional.get();
    pet.setName(petName);
    pet.setStatus(petStatus);
    petRepository.save(pet);
    return new ResponseEntity("Pet aggiornato con successo", HttpStatus.OK);
  }


  @DeleteMapping(value = "/{petId}")
  @ApiOperation(value = "Cancella un pet")
  @ApiResponses(value = {@ApiResponse(code = 404, message = "Pet non trovato"),@ApiResponse(code = 400, message = "Id non valido")})
  public ResponseEntity deletePet(@PathVariable("petId") @ApiParam("id del pet da rimuovere dallo store")
                                    @Min(value = 1, message="petId deve essere maggiore di 1") Integer petId){
    if (!petRepository.existsById(petId))
      return new ResponseEntity("Pet non trovato", HttpStatus.NOT_FOUND);
    petRepository.deleteById(petId);
    return new ResponseEntity("Pet cancellato con successo", HttpStatus.OK);
  }


  @GetMapping(value = "/findByStatus")
  @ApiOperation(value = "Trova un pet in base allo stato")
  @ApiResponses(value = {@ApiResponse(code = 404, message = "Pet non trovato"),@ApiResponse(code = 400, message = "Stato non valido")})
  @ApiImplicitParam(name = "status", value = "status filter",  allowableValues="available,pending,sold", paramType = "query", allowMultiple = true)
  public ResponseEntity findByStatus(@ApiParam("stato del pet da ricercare nello store") String status){

    //log.info(status);

    String[] allowableValues = {"available","pending","sold"};
    List<String> allowVals = new ArrayList(Arrays.asList(allowableValues));

    String[] params = status.split(",");
    List<String> paramVals = new ArrayList(Arrays.asList(params));
    log.info(paramVals.toString());

    for (String param : paramVals)
      if (!allowVals.contains(param))
        return new ResponseEntity("Stato non valido", HttpStatus.BAD_REQUEST);

    Optional<PetEntity[]> optional = petRepository.findByStatusIn(paramVals);
    if (!optional.isPresent())
      return new ResponseEntity("Pet non trovato", HttpStatus.NOT_FOUND);
    return new ResponseEntity(optional.get(), HttpStatus.OK);
  }


  @GetMapping(value = "/findByTags")
  @ApiOperation(value = "Trova un pet in base al tag")
  @ApiResponses(value = {@ApiResponse(code = 404, message = "Pet non trovato")})
  @Deprecated
  public ResponseEntity getByTags(@ApiParam("tag dei pet da ricercare nello store") @RequestParam("tag") String[] tags){
    Optional<PetEntity[]> optional = petRepository.findByTagsIn(tags);
    if (!optional.isPresent())
      return new ResponseEntity("Pet non trovato", HttpStatus.NOT_FOUND);
    return new ResponseEntity(optional.get(), HttpStatus.OK);
  }


  @PostMapping(value = "/{petId}/uploadImage")
  @ApiOperation(value = "Carica un'immagine per un pet")
  @ApiResponses(value = {@ApiResponse(code = 404, message = "Pet non trovato")})
  public ResponseEntity uploadImage(@PathVariable("petId") @ApiParam("id del pet da aggiornare")Integer petId, @ApiParam("immagine del pet")
                                @RequestParam String photoUrls){
    Optional<PetEntity> optional = petRepository.findById(petId);
    if (!optional.isPresent())
      return new ResponseEntity("Pet non trovato", HttpStatus.NOT_FOUND);
    PetEntity pet = optional.get();
    pet.setPhotoUrls(photoUrls);
    petRepository.save(pet);
    return new ResponseEntity("Immagine aggiornata con successo", HttpStatus.OK);
  }


  @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
  //@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
    Map<String, String> errors = new HashMap<>();
    ex.getBindingResult().getAllErrors().forEach((error) -> {
      String fieldName = ((FieldError) error).getField();
      //String errorMessage = error.getDefaultMessage();
      String errorMessage = "Dati non validi";
      errors.put(fieldName, errorMessage);
    });
    return errors;
  }


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {ConstraintViolationException.class})
  public Map<String, String> handleConstraintViolationExceptions(ConstraintViolationException ex) {
    Map<String, String> errors = new HashMap<>();
    ex.getConstraintViolations().forEach((error) -> {
        String fieldName = ((ConstraintViolation) error).getMessage();
        String errorMessage = "Parametro non valido";
        errors.put(fieldName, errorMessage);
      });
    return errors;
  }


}




