package it.nesea.petstore.restController;

import io.swagger.annotations.*;
import it.nesea.petstore.entity.OrderEntity;
import it.nesea.petstore.entity.PetGrouppedStatusCount;
import it.nesea.petstore.repository.StoreRepository;
import it.nesea.petstore.repository.PetRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.*;


@RestController
@RequestMapping("/store")
@Slf4j
@Api(value = "/store", description = "Accesso agli ordini del Petstore" , tags = { "store" })
@Validated
public class StoreController {


  @Autowired
  private StoreRepository storeRepository;

  @Autowired
  private PetRepository petRepository;


  @GetMapping(value = "/inventory")
  @ApiOperation(value = "Restituisce un inventario per stato dei pet")
  public ResponseEntity inventory(){
    List<PetGrouppedStatusCount> petGrouppedStatusCount = petRepository.petGrouppedStatusCount();
    return ResponseEntity.ok(petGrouppedStatusCount);
  }


  /*
  //Alternativa che usa una Map anziché un VO
  @GetMapping(value = "/inventory")
  @ApiOperation(value = "Restituisce un inventario per stato")
  public List<Map<String,String>> inventory2(){
    List<Map<String,String>> petGrouppedStatusCount = petRepository.petGrouppedStatusCount2();
    return petGrouppedStatusCount;
  }*/


  @PostMapping
  @ApiOperation(value = "Inserisce un ordine per un pet")
  @ApiResponses(value = {@ApiResponse(code = 400, message = "Dati non validi"), @ApiResponse(code = 405, message = "Ordine già esistente")})
  public ResponseEntity Order(@RequestBody @Valid @ApiParam("ordine da inserire per l'acquisto del pet") OrderEntity order){
    if (storeRepository.existsById(order.getId()))
      return new ResponseEntity("Ordine già esistente", HttpStatus.METHOD_NOT_ALLOWED);
    storeRepository.save(order);
    return new ResponseEntity("Ordine creato con successo", HttpStatus.CREATED);
  }


  @GetMapping(value = "/order/{orderId}")
  @ApiOperation(value = "Trova un ordine di acquisto tramite Id")
  @ApiResponses(value = {@ApiResponse(code = 400, message = "Id non valido")})
  public ResponseEntity getOrder(@PathVariable("orderId") @ApiParam("Id dell'ordine di acquisto del pet da ricercare")
                                   @Min(value = 1, message="orderId deve essere maggiore di 1") Integer orderId){
    Optional<OrderEntity> optional = storeRepository.findById(orderId);
    if (!optional.isPresent())
      return new ResponseEntity("Ordine non trovato", HttpStatus.NOT_FOUND);
    return new ResponseEntity(optional.get(), HttpStatus.OK);
  }


  @DeleteMapping(value = "/order/{orderId}")
  @ApiOperation(value = "Cancella un ordine di acquisto tramite Id")
  @ApiResponses(value = {@ApiResponse(code = 400, message = "Id non valido"),@ApiResponse(code = 404, message = "Ordine non trovato")})
  public ResponseEntity deleteOrder(@PathVariable("orderId") @ApiParam("Id dell'ordine di acquisto del pet da cancellare")
                                      @Min(value = 1, message="orderId deve essere maggiore di 1") Integer orderId){
    if (!storeRepository.existsById(orderId))
      return new ResponseEntity("Ordine non trovato", HttpStatus.NOT_FOUND);
    storeRepository.deleteById(orderId);
    return new ResponseEntity("Ordine cancellato con successo", HttpStatus.OK);
  }


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  //@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
  //@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
    Map<String, String> errors = new HashMap<>();
    ex.getBindingResult().getAllErrors().forEach((error) -> {
      String fieldName = ((FieldError) error).getField();
      //String errorMessage = error.getDefaultMessage();
      String errorMessage = "Dati non validi";
      errors.put(fieldName, errorMessage);
    });
    return errors;
  }


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {ConstraintViolationException.class})
  public Map<String, String> handleConstraintViolationExceptions(ConstraintViolationException ex) {
    Map<String, String> errors = new HashMap<>();
    ex.getConstraintViolations().forEach((error) -> {
      String fieldName = ((ConstraintViolation) error).getMessage();
      String errorMessage = "Parametro non valido";
      errors.put(fieldName, errorMessage);
    });
    return errors;
  }


}




