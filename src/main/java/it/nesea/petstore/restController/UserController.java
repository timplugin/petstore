package it.nesea.petstore.restController;

import io.swagger.annotations.*;
import it.nesea.petstore.entity.UserEntity;
import it.nesea.petstore.repository.UserRepository;
import it.nesea.petstore.security.SecurityConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.*;


@RestController
@RequestMapping("/user")
@Slf4j
@Api(value = "/user", description = "Operazioni per gli utenti" , tags = { "user" })
@Validated
public class UserController {


  @Autowired
  private UserRepository userRepository;

  @Autowired
  PasswordEncoder passwordEncoder;


  @PostMapping
  @ApiOperation(value = "Crea un nuovo utente")
  @ApiResponses(value = {@ApiResponse(code = 400, message = "Dati non validi"), @ApiResponse(code = 405, message = "Username già esistente"),
                      @ApiResponse(code = 406, message = "Id già utilizzato")})
  public ResponseEntity createUser(@RequestBody @Valid @ApiParam("Utente da creare") UserEntity user) {
    if (userRepository.findByUsername(user.getUsername()).isPresent())
       return new ResponseEntity("Username già esistente", HttpStatus.METHOD_NOT_ALLOWED);
    if (userRepository.existsById(user.getId()))
      return new ResponseEntity("Id già utilizzato", HttpStatus.NOT_ACCEPTABLE);
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    userRepository.save(user);
    return new ResponseEntity("Utente creato con successo", HttpStatus.CREATED);
  }


  @GetMapping(value = "/{username}")
  @ApiOperation(value = "Ricerca utente tramite username")
  public ResponseEntity findByUsername(@PathVariable("username") @ApiParam("Username dell'utente da ricercare") String username) {
    Optional<UserEntity> optional = userRepository.findByUsername(username);
    if (!optional.isPresent())
      return new ResponseEntity("Utente non trovato", HttpStatus.NOT_FOUND);
    return new ResponseEntity(optional.get(), HttpStatus.OK);
  }


  @PutMapping(value = "/{username}")
  @ApiOperation(value = "Aggiorna utente tramite username")
  @ApiResponses(value = {@ApiResponse(code = 400, message = "Dati non validi")})
  public ResponseEntity updateUser(@PathVariable("username") @ApiParam("Username dell'utente da aggiornare") String username,
                               @RequestBody @ApiParam("Dati dell'utente da aggiornare") @Valid UserEntity user) {
    Optional<UserEntity> optional = userRepository.findByUsername(username);
    if (!optional.isPresent())
      return new ResponseEntity("Utente non trovato", HttpStatus.NOT_FOUND);
    if (!optional.get().getId().equals(user.getId()))
      return new ResponseEntity("Non è permesso alterare l'id dell'utente", HttpStatus.BAD_REQUEST);
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    userRepository.save(user);
    return new ResponseEntity("Utente aggiornato con successo", HttpStatus.OK);
  }


  @Transactional
  @DeleteMapping(value = "/{username}")
  @ApiOperation(value = "Cancella utente tramite username")
  public ResponseEntity deleteByUsername(@PathVariable("username") @ApiParam("Username dell'utente da cancellare") String username) {
    Optional<UserEntity> optional = userRepository.findByUsername(username);
    if (!optional.isPresent())
      return new ResponseEntity("Utente non trovato", HttpStatus.NOT_FOUND);
    if (optional.get().getId().equals(1))
      return new ResponseEntity("Non è possibile cancellare l'utente admin", HttpStatus.METHOD_NOT_ALLOWED);
    userRepository.deleteByUsername(username);
    return new ResponseEntity("Utente cancellato con successo", HttpStatus.OK);
  }


  @PostMapping("/createWithArray")
  @ApiOperation(value = "Crea utenti tramite Array")
  @ApiResponses(value = {@ApiResponse(code = 400, message = "Dati non validi"), @ApiResponse(code = 405, message = "Username già esistente"),
                      @ApiResponse(code = 406, message = "Id già utilizzato")})
  public ResponseEntity createWithArray(@RequestBody @Valid @ApiParam("Array degli utenti da creare") UserEntity[] users) {

    int n=0;
    for (UserEntity user : users) {
      if (userRepository.findByUsername(user.getUsername()).isPresent())
        return new ResponseEntity("Username già esistente", HttpStatus.METHOD_NOT_ALLOWED);
      if (userRepository.existsById(user.getId()))
        return new ResponseEntity("Id già utilizzato", HttpStatus.NOT_ACCEPTABLE);
    }

    for(int i=0; i<users.length; i++){
      for(int j=i+1; j<users.length; j++) {
        if (users[i].getUsername().equals(users[j].getUsername()))
          return new ResponseEntity("Username duplicato in lista", HttpStatus.METHOD_NOT_ALLOWED);
        if (users[i].getId().equals(users[j].getId()))
          return new ResponseEntity("Id duplicato in lista", HttpStatus.NOT_ACCEPTABLE);
      }
    }

    for (UserEntity user : users) {
      user.setPassword(passwordEncoder.encode(user.getPassword()));
      userRepository.save(user);
    }

    return new ResponseEntity("Utenti creati con successo", HttpStatus.CREATED);
  }


  @PostMapping("/createWithList")
  @ApiOperation(value = "Crea utenti tramite Lista")
  @ApiResponses(value = {@ApiResponse(code = 400, message = "Dati non validi"), @ApiResponse(code = 405, message = "Username già esistente"),
                    @ApiResponse(code = 406, message = "Id già utilizzato")})
  public ResponseEntity createWithList(@RequestBody @Valid @ApiParam("Lista degli utenti da creare") List<UserEntity> users) {

    for (UserEntity user : users) {
      if (userRepository.findByUsername(user.getUsername()).isPresent())
        return new ResponseEntity("Username già esistente", HttpStatus.METHOD_NOT_ALLOWED);
      if (userRepository.existsById(user.getId()))
        return new ResponseEntity("Id già utilizzato", HttpStatus.NOT_ACCEPTABLE);
    }

    for(int i=0; i<users.size(); i++){
      for(int j=i+1; j<users.size(); j++) {
        if (users.get(i).getUsername().equals(users.get(j).getUsername()))
          return new ResponseEntity("Username duplicato in lista", HttpStatus.METHOD_NOT_ALLOWED);
        if (users.get(i).getId().equals(users.get(j).getId()))
          return new ResponseEntity("Id duplicato in lista", HttpStatus.NOT_ACCEPTABLE);
      }
    }
    for (UserEntity user : users) {
      user.setPassword(passwordEncoder.encode(user.getPassword()));
      userRepository.save(user);
    }

    return new ResponseEntity("Utenti creati con successo", HttpStatus.CREATED);
  }


  @GetMapping("/login")
  @ApiOperation(value = "Effettua la login dell'utente")
  public ResponseEntity login(@RequestParam @ApiParam("Username") String username, @RequestParam @ApiParam("Password")String password) {
    //log.info("+username:" + username);
    //log.info("+password:" + password);
    return ResponseEntity.ok().build();
  }


  @GetMapping("/logout")
  @ApiOperation(value = "Effettua la logout dell'utente")
  public ResponseEntity logout(HttpSession session, HttpServletRequest request) {

    //implemento una blacklist in session. (con il db H2 si avrebbe sempre lo stesso effetto)
    String token = request.getHeader(SecurityConstants.TOKEN_HEADER);
    Set<String> blacklist = (Set) session.getAttribute("blacklist");
    if (blacklist == null) {
      blacklist = new HashSet<String>();
      session.setAttribute("blacklist",blacklist);
    }
    blacklist.add(token);
    return ResponseEntity.ok().build();
  }


  //per utilità
  @GetMapping("/findAll")
  @ApiOperation(value = "Elenca tutti gli utenti esistenti (utility)")
  public ResponseEntity findAll() {
    return ResponseEntity.ok(userRepository.findAll());
  }


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  //@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
  //@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
    Map<String, String> errors = new HashMap<>();
    ex.getBindingResult().getAllErrors().forEach((error) -> {
      String fieldName = ((FieldError) error).getField();
      //String errorMessage = error.getDefaultMessage();
      String errorMessage = "Dati non validi";
      errors.put(fieldName, errorMessage);
    });
    return errors;
  }


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {ConstraintViolationException.class})
  public Map<String, String> handleConstraintViolationExceptions(ConstraintViolationException ex) {
    Map<String, String> errors = new HashMap<>();
    ex.getConstraintViolations().forEach((error) -> {
      String fieldName = ((ConstraintViolation) error).getMessage();
      String errorMessage = "Parametro non valido";
      errors.put(fieldName, errorMessage);
    });
    return errors;
  }



}




