package it.nesea.petstore;

import it.nesea.petstore.entity.UserEntity;
import it.nesea.petstore.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@EnableSwagger2
@EnableWebSecurity
@Slf4j
public class PetstoreApplication implements CommandLineRunner {

  @Autowired
  UserRepository userRepository;

  @Autowired
  PasswordEncoder passwordEncoder;


  public static void main(String[] args) {
		SpringApplication.run(PetstoreApplication.class, args);
  }

  @Bean
  public Docket api() {
	return new Docket(DocumentationType.SWAGGER_2)
	   .select()
	   .apis(RequestHandlerSelectors.basePackage( "it.nesea"))
	   .paths(PathSelectors.any())
	   .build();
  }

  //@Bean
  //public MethodValidationPostProcessor methodValidationPostProcessor() {
	//return new MethodValidationPostProcessor();
  //}

  @Override
  public void run(String... args) throws Exception {

	UserEntity user = new UserEntity();
	user.setId(1);
	user.setUsername("admin");
	user.setPassword(passwordEncoder.encode("admin"));
	user.setFirstName("admin");
	user.setLastName("admin");
	user.setEmail("petstoreAdmin@nesea.it");
	user.setPhone("555-111-555");
	user.setUserStatus(1);
	userRepository.save(user);

	log.info("RUN");



  }
}
